import 'dart:html';
import 'package:flutter/material.dart';
import 'package:reg_ui/homePage.dart';
import 'drawer.dart';

class LoginPage extends StatelessWidget {
  static const String routePage = '/loginPage';

  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ConstrainedBox(
        constraints: const BoxConstraints(maxWidth: 300.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            // const SizedBox(height: 50.0),
            const Spacer(flex: 5),
            // const FlutterLogo(size: 100.0),
            Expanded(
              child: Image.asset("images/buu_logo.png"), 
              flex: 10
              ),
            const Spacer(flex: 3),
            // const SizedBox(height: 100.0),
            const TextField(decoration: InputDecoration(labelText: 'รหัสนิสิต')),
            const TextField(decoration: InputDecoration(labelText: 'รหัสผ่าน')),
            // const SizedBox(height: 30.0),
            const Spacer(flex: 3),
            ElevatedButton(onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) => homePage()));
            }, child: const Text('LOGIN')),
            const Spacer(flex: 20),
          ],
        ),
      ),
    );
  }
}
