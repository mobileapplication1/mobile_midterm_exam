import 'package:flutter/material.dart';

class gradeStu extends StatelessWidget{
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.amber,
        title: Text("ผลการศึกษา"),
        elevation: 0.0,
      ),
      body: ListView(
        children: <Widget>[
          Container(
            child: Image.network("https://lh3.googleusercontent.com/a/AEdFTp7Er8hxlRTlgv_hpVFI6Uvrf-aCKSHeVDSfkHlT8g=s288-p-rw-no",
            fit: BoxFit.cover,
            width: 350,
            height: 350,
            ),
          ),
          Container(
            child: Text("63160183: นายกษมา จำนงค์ลาภ: คณะวิทยาการสารสนเทศ",
            style: TextStyle(fontSize: 14, color: Colors.black,)
            ),
          ),
          Container(
            child: Text("สถานภาพ: กำลังศึกษา",
            style: TextStyle(fontSize: 14, color: Colors.black,)
            ),
          ),
          Container(
            child: Text("อ. ที่ปรึกษา: อาจารย์ภูสิต กุลเกษม,ผู้ช่วยศาสตราจารย์ ดร.โกเมศ อัมพวัน",
            style: TextStyle(fontSize: 14, color: Colors.black,)
            ),
          ),
          Container(
            child: Text("ภาคการศึกษาที่ 1 / 2565",
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 20, color: Colors.orange,)
            ),
          ),
          Container(
            color: Colors.white,
            padding: EdgeInsets.all(20.0),
            child: Table(
              children: const [
                TableRow(children: [
                  Text(
                    "รหัสวิชา",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "ชื่อรายวิชา",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "หน่วยกิต",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "เกรด",
                    style: TextStyle(fontSize: 15.0),
                  ),
                ]),
                TableRow(children: [
                  Text(
                    "67511164",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "CPSAT",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "3",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "A",
                    style: TextStyle(fontSize: 15.0),
                  ),
                ]),
                TableRow(children: [
                  Text(
                    "88624259",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "Mobile Paradigm",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "3",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "B",
                    style: TextStyle(fontSize: 15.0),
                  ),
                ]),
                TableRow(children: [
                  Text(
                    "88631159",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "Alogrithm",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "3",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "D+",
                    style: TextStyle(fontSize: 15.0),
                  ),
                ]),
                TableRow(children: [
                  Text(
                    "88633159",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "ComNet",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "3",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "C+",
                    style: TextStyle(fontSize: 15.0),
                  ),
                ]),
                TableRow(children: [
                  Text(
                    "88634159",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "SoftDev",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "3",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "B",
                    style: TextStyle(fontSize: 15.0),
                  ),
                ]),
                TableRow(children: [
                  Text(
                    "88635359",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "UI Design",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "3",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "C+",
                    style: TextStyle(fontSize: 15.0),
                  ),
                ]),
                TableRow(children: [
                  Text(
                    "88636159",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "Intro to AI",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "3",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "C",
                    style: TextStyle(fontSize: 15.0),
                  ),
                ]),
              ],
            ),
          ),
          Container(
            child: Text("ภาคการศึกษาที่ 2 / 2565",
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 20, color: Colors.orange,)
            ),
          ),
          Container(
            color: Colors.white,
            padding: EdgeInsets.all(20.0),
            child: Table(
              children: const [
                TableRow(children: [
                  Text(
                    "รหัสวิชา",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "ชื่อรายวิชา",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "หน่วยกิต",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "เกรด",
                    style: TextStyle(fontSize: 15.0),
                  ),
                ]),
                TableRow(children: [
                  Text(
                    "88624359",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "Web Programming",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "3",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "B+",
                    style: TextStyle(fontSize: 15.0),
                  ),
                ]),
                TableRow(children: [
                  Text(
                    "88624459",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "OOAD",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "3",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "B+",
                    style: TextStyle(fontSize: 15.0),
                  ),
                ]),
                TableRow(children: [
                  Text(
                    "88624559",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "Software Testing",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "3",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "C+",
                    style: TextStyle(fontSize: 15.0),
                  ),
                ]),
                TableRow(children: [
                  Text(
                    "88634259",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "Multimedia",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "3",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "A",
                    style: TextStyle(fontSize: 15.0),
                  ),
                ]),
                TableRow(children: [
                  Text(
                    "88634459",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "Mobile Application I",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "3",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "Ultimate A+",
                    style: TextStyle(fontSize: 15.0),
                  ),
                ]),
                TableRow(children: [
                  Text(
                    "88646259",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "Intro to NLP",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "3",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "D+",
                    style: TextStyle(fontSize: 15.0),
                  ),
                ]),
              ],
            ),
          ),
          Container(
            child: Text("การแสดงเกรด",
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 20, color: Colors.red,)
            ),
          ),
          Container(
            child: Text("1. กรณีที่นิสิตประเมินครบทุกรายวิชา และเกรดออกแล้ว ช่องแสดงเกรดจะเป็นเกรดที่ได้",
            style: TextStyle(fontSize: 14, color: Colors.red,)
            ),
          ),
          Container(
            child: Text("2. กรณีที่นิสิตประเมินครบทุกวิชาแล้ว แต่เกรดยังไม่ออก ช่องแสดงเกรดจะเป็นช่องว่าง",
            style: TextStyle(fontSize: 14, color: Colors.red,)
            ),
          ),
          Container(
            child: Text("3. กรณีที่นิสิตประเมินไม่ครบทุกวิชา และเกรดออกแล้ว ช่องแสดงเกรดจะมีเครื่องหมาย ??",
            style: TextStyle(fontSize: 14, color: Colors.red,)
            ),
          ),
        ],
      ),
    );
  }
}