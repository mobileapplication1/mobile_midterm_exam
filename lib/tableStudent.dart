import 'package:flutter/material.dart';

class timeTable extends StatelessWidget {
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.amber,
        title: Text("ตารางเรียน/สอบ"),
        elevation: 0.0,
      ),
      body: ListView(
        children: <Widget>[
          Container(
            child: Text("ตารางเรียน/สอบของรายวิชาที่ลงทะเบียนไว้แล้ว",
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 16, color: Colors.orange,)
            ),
          ),
          Container(
            color: Colors.white,
            padding: EdgeInsets.all(20.0),
            child: Table(
              children: const [
                TableRow(children: [
                  Text(
                    "ชื่อ",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "กษมา จำนงค์ลาภ",
                    style: TextStyle(fontSize: 15.0),
                  ),
                ]),
                TableRow(children: [
                  Text(
                    "สถานภาพ",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "กำลังศึกษา",
                    style: TextStyle(fontSize: 15.0),
                  ),
                ]),
                TableRow(children: [
                  Text(
                    "คณะ",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "วิทยาการสารสนเทศ",
                    style: TextStyle(fontSize: 15.0),
                  ),
                ]),
                TableRow(children: [
                  Text(
                    "อ.ที่ปรึกษา",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "อาจารย์ภูสิต กุลเกษม,ผู้ช่วยศาสตราจารย์ ดร.โกเมศ อัมพวัน",
                    style: TextStyle(fontSize: 15.0),
                  ),
                ]),
              ],
            ),
          ),
          Container(
            child: Image.network("https://scontent.fbkk20-1.fna.fbcdn.net/v/t1.15752-9/327483997_496186259253564_5547838248023551338_n.png?_nc_cat=104&ccb=1-7&_nc_sid=ae9488&_nc_eui2=AeF61atff2ZFwLu3Z_FNoz0lBZtrfQ1hyXUFm2t9DWHJdSkCDux-ks8MAcDcMVHFNFw8IwO-hdCRS7xCUJDb_8M_&_nc_ohc=kVxPNOVVv_AAX9IXgk8&_nc_ht=scontent.fbkk20-1.fna&oh=03_AdSJbXEfAF-5tarZ9WbecqRmDgWu5UTGTZmHe-Pt0mAN-g&oe=63F903DA",
            fit: BoxFit.cover,
            ),
          ),
          Container(
            child: Text("ตารางสอบ",
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 20, color: Colors.orange,)
            ),
          ),
          Container(
            child: Image.network("https://scontent.fbkk20-1.fna.fbcdn.net/v/t1.15752-9/327429068_886051066064575_1725971914311882711_n.png?_nc_cat=106&ccb=1-7&_nc_sid=ae9488&_nc_eui2=AeGqgz2lT5jjwBz0TziBX5J9kO3VqLxMYQaQ7dWovExhBgiKw3oA-IB71SjWwopGYztjVIPyYNeztRNUTkLLEeHy&_nc_ohc=imDK68xfo5kAX9LdfNt&tn=kK0Y1B0JVdpq34ta&_nc_ht=scontent.fbkk20-1.fna&oh=03_AdTHjqkPxOD3nD2-Y4lDay-Ng84RKhiJnXkEjctpNDjGRQ&oe=63F8E8A5",
            fit: BoxFit.cover,
            ),
          ),
        ],
      ),
    );
  }
}