import 'package:flutter/material.dart';

class registTable extends StatelessWidget{
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.amber,
        title: Text("ผลการลงทะเบียน"),
        elevation: 0.0,
      ),
      body: ListView(
        children: <Widget>[
          Container(
            child: Text("ปีการศึกษา 2565 / 2",
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 15, color: Colors.black,)
            ),
          ),
          Container(
            child: Text("รายวิชาที่ลงทะเบียนทั้งหมด",
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 20, color: Colors.black,)
            ),
          ),
          Container(
            color: Colors.white,
            padding: EdgeInsets.all(20.0),
            child: Table(
              children: const [
                TableRow(children: [
                  Text(
                    "รหัสวิชา",
                    style: TextStyle(fontSize: 11.0),
                  ),
                  Text(
                    "ชื่อรายวิชา",
                    style: TextStyle(fontSize: 11.0),
                  ),
                  Text(
                    "แบบการศึกษา",
                    style: TextStyle(fontSize: 11.0),
                  ),
                  Text(
                    "หน่วยกิต",
                    style: TextStyle(fontSize: 11.0),
                  ),
                  Text(
                    "กลุ่ม",
                    style: TextStyle(fontSize: 11.0),
                  ),
                ]),
                TableRow(children: [
                  Text(
                    "88624359",
                    style: TextStyle(fontSize: 11.0),
                  ),
                  Text(
                    "Web Programming",
                    style: TextStyle(fontSize: 11.0),
                  ),
                  Text(
                    "GD",
                    style: TextStyle(fontSize: 11.0),
                  ),
                  Text(
                    "3",
                    style: TextStyle(fontSize: 11.0),
                  ),
                  Text(
                    "1",
                    style: TextStyle(fontSize: 11.0),
                  ),
                ]),
                TableRow(children: [
                  Text(
                    "88624459",
                    style: TextStyle(fontSize: 11.0),
                  ),
                  Text(
                    "OOAD",
                    style: TextStyle(fontSize: 11.0),
                  ),
                  Text(
                    "GD",
                    style: TextStyle(fontSize: 11.0),
                  ),
                  Text(
                    "3",
                    style: TextStyle(fontSize: 11.0),
                  ),
                  Text(
                    "1",
                    style: TextStyle(fontSize: 11.0),
                  ),
                ]),
                TableRow(children: [
                  Text(
                    "88624559",
                    style: TextStyle(fontSize: 11.0),
                  ),
                  Text(
                    "Software Testing",
                    style: TextStyle(fontSize: 11.0),
                  ),
                  Text(
                    "GD",
                    style: TextStyle(fontSize: 11.0),
                  ),
                  Text(
                    "3",
                    style: TextStyle(fontSize: 11.0),
                  ),
                  Text(
                    "1",
                    style: TextStyle(fontSize: 11.0),
                  ),
                ]),
                TableRow(children: [
                  Text(
                    "88634259",
                    style: TextStyle(fontSize: 11.0),
                  ),
                  Text(
                    "Multi",
                    style: TextStyle(fontSize: 11.0),
                  ),
                  Text(
                    "GD",
                    style: TextStyle(fontSize: 11.0),
                  ),
                  Text(
                    "3",
                    style: TextStyle(fontSize: 11.0),
                  ),
                  Text(
                    "1",
                    style: TextStyle(fontSize: 11.0),
                  ),
                ]),
                TableRow(children: [
                  Text(
                    "88634459",
                    style: TextStyle(fontSize: 11.0),
                  ),
                  Text(
                    "Mobile Application 1",
                    style: TextStyle(fontSize: 11.0),
                  ),
                  Text(
                    "GD",
                    style: TextStyle(fontSize: 11.0),
                  ),
                  Text(
                    "3",
                    style: TextStyle(fontSize: 11.0),
                  ),
                  Text(
                    "1",
                    style: TextStyle(fontSize: 11.0),
                  ),
                ]),
                TableRow(children: [
                  Text(
                    "88646259",
                    style: TextStyle(fontSize: 11.0),
                  ),
                  Text(
                    "Intro to NLP",
                    style: TextStyle(fontSize: 11.0),
                  ),
                  Text(
                    "GD",
                    style: TextStyle(fontSize: 11.0),
                  ),
                  Text(
                    "3",
                    style: TextStyle(fontSize: 11.0),
                  ),
                  Text(
                    "1",
                    style: TextStyle(fontSize: 11.0),
                  ),
                ]),
              ],
            ),
          ),
          Container(
            child: Text("จำนวนหน่วยกิตรวม: 18",
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 20, color: Colors.black,)
            ),
          ),
        ],
      ),
    );
  }
}