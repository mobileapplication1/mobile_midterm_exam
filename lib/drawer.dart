import 'package:flutter/material.dart';
import 'package:reg_ui/bioStu.dart';
import 'package:reg_ui/loginPage.dart';
import 'package:reg_ui/registStudy.dart';
import 'package:reg_ui/resultGrade.dart';
import 'package:reg_ui/tableStudent.dart';

class navDrawer extends StatelessWidget{
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          UserAccountsDrawerHeader(
                  accountName: Text("กษมา จำนงค์ลาภ"),
                  accountEmail: Text("63160183@go.buu.ac.th"),
                  currentAccountPicture: CircleAvatar(
                    backgroundImage: NetworkImage(
                        "https://lh3.googleusercontent.com/a/AEdFTp7Er8hxlRTlgv_hpVFI6Uvrf-aCKSHeVDSfkHlT8g=s288-p-rw-no"),
                  ),
                  decoration: BoxDecoration(color: Colors.amber),
                ),
                ListTile(
                  leading: Icon(Icons.person),
                  title: const Text('ประวัตินิสิต'),
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => bioMe()));
                  },
                ),
                ListTile(
                  leading: Icon(Icons.table_chart),
                  title: const Text('ตารางเรียน/สอบ'),
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => timeTable()));
                  },
                ),
                ListTile(
                  leading: Icon(Icons.book),
                  title: const Text('ผลการศึกษา'),
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => gradeStu()));
                  },
                ),
                ListTile(
                  leading: Icon(Icons.app_registration),
                  title: const Text('ผลการลงทะเบียน'),
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => registTable()));
                  },
                ),
        ],
      ),
    );
  }
}