import 'package:flutter/material.dart';
import 'package:reg_ui/drawer.dart';
import 'package:carousel_slider/carousel_slider.dart';

class homePage extends StatelessWidget {
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Burapha University'),
        elevation: 0.0,
      ),
      drawer: navDrawer(),
      body: ListView(
        children: <Widget>[
          Container(
            child: Text(
              "ประกาศข่าวสารต่างๆ",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 20,
                color: Colors.orange,
              ),
            ),
          ),
          CarouselSlider(
            items: [
              Container(
                margin: EdgeInsets.all(8.0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  image: DecorationImage(
                    image: AssetImage("assets/images/grad652.png"),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.all(8.0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  image: DecorationImage(
                    image: AssetImage("assets/images/pay652.jpg"),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.all(8.0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  image: DecorationImage(
                    image: AssetImage("assets/images/grad653.jpg"),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.all(8.0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  image: DecorationImage(
                    image: AssetImage("assets/images/didBUU.jpg"),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.all(8.0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  image: DecorationImage(
                    image: AssetImage("assets/images/bcBUU.jpg"),
                  ),
                ),
              ),
            ],
            options: CarouselOptions(
              height: 380.0,
              enlargeCenterPage: true,
              autoPlay: true,
              aspectRatio: 16 / 9,
              autoPlayCurve: Curves.fastOutSlowIn,
              enableInfiniteScroll: true,
              autoPlayAnimationDuration: Duration(milliseconds: 800),
              viewportFraction: 0.8,
            ),
          ),
          Container(
            child: Text(
              "ประกาศเรื่อง",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 20,
                color: Colors.orange,
              ),
            ),
          ),
          Container(
            child: Text(
              "แบบประเมินความคิดเห็นต่อการให้บริการของสำนักงานอธิการบดี",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 14,
                color: Colors.black,
              ),
            ),
          ),
          Container(
            child: Image.asset("assets/images/632ass.jpg",
            fit: BoxFit.cover,
            ),
          ),
          Container(
            child: Text(
              "LINE Official ของกองทะเบียนฯ",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 14,
                color: Colors.black,
              ),
            ),
          ),
          Container(
            child: Image.asset("assets/images/Line.jpg",
            fit: BoxFit.cover,
            ),
          ),
        ],
      ),
    );
  }
}
