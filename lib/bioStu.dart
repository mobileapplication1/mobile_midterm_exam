import 'package:flutter/material.dart';

class bioMe extends StatelessWidget {
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.amber,
        title: Text("ประวัตินิสิต"),
        elevation: 0.0,
      ),
      body: ListView(
        children: <Widget>[
          Container(
            child: Image.network("https://lh3.googleusercontent.com/a/AEdFTp7Er8hxlRTlgv_hpVFI6Uvrf-aCKSHeVDSfkHlT8g=s288-p-rw-no",
            fit: BoxFit.cover,
            width: 350,
            height: 350,
            ),
          ),
          Container(
            child: Text("ข้อมูลด้านการศึกษา",
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 20, color: Colors.black,)
            ),
          ),
          Container(
            color: Colors.white,
            padding: EdgeInsets.all(20.0),
            child: Table(
              children: const [
                TableRow(children: [
                  Text(
                    "รหัสประจำตัว",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "63160183",
                    style: TextStyle(fontSize: 15.0),
                  ),
                ]),
                TableRow(children: [
                  Text(
                    "ชื่อ",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "นายกษมา จำนงค์ลาภ",
                    style: TextStyle(fontSize: 15.0),
                  ),
                ]),
                TableRow(children: [
                  Text(
                    "ชื่ออังกฤษ",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "MR.KASAMA JAMNONGLAP",
                    style: TextStyle(fontSize: 15.0),
                  ),
                ]),
                TableRow(children: [
                  Text(
                    "คณะ",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "คณะวิทยาการสารสนเทศ",
                    style: TextStyle(fontSize: 15.0),
                  ),
                ]),
                TableRow(children: [
                  Text(
                    "วิทยาเขต",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "บางแสน",
                    style: TextStyle(fontSize: 15.0),
                  ),
                ]),
                TableRow(children: [
                  Text(
                    "หลักสูตร",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "2115020 วท.บ. (วิทยาการคอมพิวเตอร์) ปรับปรุง 59 - ป.ตรี 4 ปี ปกติ",
                    style: TextStyle(fontSize: 15.0),
                  ),
                ]),
                TableRow(children: [
                  Text(
                    "ระดับการศึกษา",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "ปริญญาตรี",
                    style: TextStyle(fontSize: 15.0),
                  ),
                ]),
                TableRow(children: [
                  Text(
                    "วิธีรับเข้า",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "รับตรงทั่วประเทศ",
                    style: TextStyle(fontSize: 15.0),
                  ),
                ]),
                TableRow(children: [
                  Text(
                    "วุฒิก่อนเข้ารับการศึกษา",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "ม.6 2.65",
                    style: TextStyle(fontSize: 15.0),
                  ),
                ]),
                TableRow(children: [
                  Text(
                    "จบการศึกษาจาก",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "โรงเรียนวชิรธรรมสาธิต",
                    style: TextStyle(fontSize: 15.0),
                  ),
                ]),
              ],
            ),
          ),
          Container(
            child: Text("ผลการศึกษา",
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 20, color: Colors.black,)
            ),
          ),
          Container(
            color: Colors.white,
            padding: EdgeInsets.all(20.0),
            child: Table(
              children: const [
                TableRow(children: [
                  Text(
                    "หน่วยกิตคำนวณ",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "96",
                    style: TextStyle(fontSize: 15.0),
                  ),
                ]),
                TableRow(children: [
                  Text(
                    "หน่วยกิตที่ผ่าน",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "93",
                    style: TextStyle(fontSize: 15.0),
                  ),
                ]),
                TableRow(children: [
                  Text(
                    "คะแนนเฉลี่ยสะสม",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "2.93",
                    style: TextStyle(fontSize: 15.0),
                  ),
                ]),
              ],
            ),
          ),
          Container(
            child: Text("ข้อมูลส่วนบุคคล",
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 20, color: Colors.black,)
            ),
          ),
          Container(
            color: Colors.white,
            padding: EdgeInsets.all(20.0),
            child: Table(
              children: const [
                TableRow(children: [
                  Text(
                    "สัญชาติ",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "ไทย",
                    style: TextStyle(fontSize: 15.0),
                  ),
                ]),
                TableRow(children: [
                  Text(
                    "ศาสนา",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "พุทธ",
                    style: TextStyle(fontSize: 15.0),
                  ),
                ]),
                TableRow(children: [
                  Text(
                    "หมู่เลือด",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "B",
                    style: TextStyle(fontSize: 15.0),
                  ),
                ]),
                TableRow(children: [
                  Text(
                    "สถานภาพบิดาและมารดา",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "มีชีวิตอยู่",
                    style: TextStyle(fontSize: 15.0),
                  ),
                ]),
                TableRow(children: [
                  Text(
                    "สภาพการสมรสบิดามารดา",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "อยู่ด้วยกัน",
                    style: TextStyle(fontSize: 15.0),
                  ),
                ]),
                TableRow(children: [
                  Text(
                    "อาชีพผู้ปกครอง",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "ค้าขาย,ธุรกิจส่วนตัวและอาชีพอิสระ/รับจ้างอิสระแบบไม่ประจำ",
                    style: TextStyle(fontSize: 15.0),
                  ),
                ]),
                TableRow(children: [
                  Text(
                    "รายได้ผู้ปกครอง",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  Text(
                    "> 300,000 บาทต่อปี (> 25,000 บาทต่อเดือน)",
                    style: TextStyle(fontSize: 15.0),
                  ),
                ]),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
